
#include "adc.h"
#include "DSP28x_Project.h"

void InitADC(void)
{
    InitAdc();  // Init the ADC
    AdcOffsetSelfCal();

    // User specific code, enable interrupts

    // Enable CPU INT1 which is connected to ADC INT
    IER |= M_INT1;

    // Enable ADC INT 1.1 in the PIE
    PieCtrlRegs.PIEIER1.bit.INTx1 = 1;

    // Enable global Interrupts and higher priority real-time debug events
    EINT; // Enable Global interrupt INTM
    ERTM; // Enable Global realtime interrupt DBGM

    // GPIO pin configuration
    EALLOW;
    GpioCtrlRegs.GPAMUX1.all = 0x00000555; // (15-6) GPIOx, (5-0) ePWMx
    GpioCtrlRegs.GPAPUD.all = 0x0000000;  // (31-0) pull-up
    GpioCtrlRegs.GPADIR.all = 0xFFFFFFFF; // (31-0) GPIO outputs
    GpioCtrlRegs.GPBDIR.all = 0xFFFFFFFF; // (31-0) GPIO outputs

    GpioCtrlRegs.GPADIR.bit.GPIO6 = 0;  // BTN1 input
    GpioCtrlRegs.GPADIR.bit.GPIO8 = 0;  // BTN2 input
    GpioCtrlRegs.GPACTRL.bit.QUALPRD3 = 1;  // Qualification period = SYSCLKOUT/2
    GpioCtrlRegs.GPAQSEL1.bit.GPIO6 = 2;   // 6 samples
    GpioCtrlRegs.GPAQSEL1.bit.GPIO8 = 2;   // 6 samples
    EDIS;

    // Configure ADC

    // ADCINA0, controlCard pin 57, Ia sensor
    // ADCINA1, controlCard pin 59, Ib sensor
    // ADCINA2, controlCard pin 61, Ic sensor
    // ADCINA3, controlCard pin 63, 1.5V voltage reference, corresponding to 0A
    // ADCINA7, controlCard pin 71, Vdc voltage divider

    EALLOW;
    AdcRegs.ADCCTL2.bit.ADCNONOVERLAP = 1; // Non-overlap mode
    AdcRegs.ADCCTL1.bit.INTPULSEPOS = 1;   // INT pulse on EOC
    AdcRegs.INTSEL1N2.bit.INT1E = 1;       // Enabled ADCINT1
    AdcRegs.INTSEL1N2.bit.INT1CONT = 0;    // Disable ADCINT1 continuous mode
    AdcRegs.INTSEL1N2.bit.INT1SEL = 4;     // Connect ADCINT1 to EOC4

    // SOC0 - this must be ignored! See silicon errata.
    AdcRegs.ADCSOC0CTL.bit.CHSEL = 0; // set SOC0 channel select to ADCINA0 = Ia
    AdcRegs.ADCSOC0CTL.bit.TRIGSEL = 5;    // set SOC0 start trigger on EPWM1A
    AdcRegs.ADCSOC0CTL.bit.ACQPS = 6; // set SOC0 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)

    // SOC1
    AdcRegs.ADCSOC1CTL.bit.CHSEL = 0; // set SOC0 channel select to ADCINA0 = Ia
    AdcRegs.ADCSOC1CTL.bit.TRIGSEL = 5;
    AdcRegs.ADCSOC1CTL.bit.ACQPS = 20; // set SOC0 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1)

    // SOC2
    AdcRegs.ADCSOC2CTL.bit.CHSEL = 1; // set SOC1 channel select to ADCINA1 = Ib
    AdcRegs.ADCSOC2CTL.bit.TRIGSEL = 5;
    AdcRegs.ADCSOC2CTL.bit.ACQPS = 20;

    // SOC3
    AdcRegs.ADCSOC3CTL.bit.CHSEL = 7; // set SOC2 channel select to ADCINA7 = VDC
    AdcRegs.ADCSOC3CTL.bit.TRIGSEL = 5;
    AdcRegs.ADCSOC3CTL.bit.ACQPS = 20;

    // SOC4
    AdcRegs.ADCSOC4CTL.bit.CHSEL = 3; // set SOC3 channel select to ADCINA3 = Vref 1.5V
    AdcRegs.ADCSOC4CTL.bit.TRIGSEL = 5;
    AdcRegs.ADCSOC4CTL.bit.ACQPS = 20;
    EDIS;

    // Configure ADC start of conversion signal
    EPwm1Regs.ETSEL.bit.SOCAEN = 1;  // Enable SOC on A group
    EPwm1Regs.ETSEL.bit.SOCASEL = ET_CTR_PRD; // TODO Check the position of SOC vs PWM signal. Low duty cycle is expected.
    EPwm1Regs.ETPS.bit.SOCAPRD = ET_1ST;
}
