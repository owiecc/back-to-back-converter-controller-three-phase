
#include "pwm.h"
#include "DSP28x_Project.h"

void InitEPwmCommon(int period, int deadband)
{
    // Configure ePWM modules

    EPwm1Regs.TBPRD = period;
    EPwm2Regs.TBPRD = period;
    EPwm3Regs.TBPRD = period;

    EPwm1Regs.CMPA.half.CMPA = period / 2;
    EPwm2Regs.CMPA.half.CMPA = period / 2;
    EPwm3Regs.CMPA.half.CMPA = period / 2;

    // Clear counters

    EPwm1Regs.TBCTR = 0;
    EPwm2Regs.TBCTR = 0;
    EPwm3Regs.TBCTR = 0;

    EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
    EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;
    EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;

    // Set TBCLK = SYSCLKOUT

    EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
    EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
    EPwm3Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;

    // Sync all PWM modules

    EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;
    EPwm2Regs.TBCTL.bit.PHSEN = TB_ENABLE;
    EPwm3Regs.TBCTL.bit.PHSEN = TB_ENABLE;
    EPwm2Regs.TBPHS.half.TBPHS = 0;
    EPwm3Regs.TBPHS.half.TBPHS = 0;

    EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;     // Master unit
    EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;
    EPwm3Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;

    // Load registers every ZERO

    EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
    EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
    EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

    EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR;  // Set actions
    EPwm1Regs.AQCTLA.bit.CAD = AQ_SET;
    EPwm2Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    EPwm2Regs.AQCTLA.bit.CAD = AQ_SET;
    EPwm3Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    EPwm3Regs.AQCTLA.bit.CAD = AQ_SET;

    // Active HI complementary PWMs + setup deadband

    EPwm1Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
    EPwm1Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
    EPwm1Regs.DBCTL.bit.IN_MODE = DBA_ALL;
    EPwm1Regs.DBRED = deadband;
    EPwm1Regs.DBFED = deadband;

    EPwm2Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
    EPwm2Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
    EPwm2Regs.DBCTL.bit.IN_MODE = DBA_ALL;
    EPwm2Regs.DBRED = deadband;
    EPwm2Regs.DBFED = deadband;

    EPwm3Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
    EPwm3Regs.DBCTL.bit.POLSEL = DB_ACTV_HIC;
    EPwm3Regs.DBCTL.bit.IN_MODE = DBA_ALL;
    EPwm3Regs.DBRED = deadband;
    EPwm3Regs.DBFED = deadband;

    // Trip action set to force-low for output A

    EALLOW;
    EPwm1Regs.TZCTL.bit.TZA = 0b10;
    EPwm2Regs.TZCTL.bit.TZA = 0b10;
    EPwm3Regs.TZCTL.bit.TZA = 0b10;

    EPwm1Regs.TZCTL.bit.TZB = 0b01;
    EPwm2Regs.TZCTL.bit.TZB = 0b01;
    EPwm3Regs.TZCTL.bit.TZB = 0b01;

    EPwm1Regs.TZSEL.bit.OSHT1 = 1;
    EPwm2Regs.TZSEL.bit.OSHT1 = 1;
    EPwm3Regs.TZSEL.bit.OSHT1 = 1;
    EDIS;

    DisablePWM();
}

void EnablePWM(void)
{
    EALLOW;
    EPwm1Regs.TZCLR.bit.OST = 0x1;
    EPwm2Regs.TZCLR.bit.OST = 0x1;
    EPwm3Regs.TZCLR.bit.OST = 0x1;
    EDIS;
}

void DisablePWM(void)
{
    EALLOW;
    EPwm1Regs.TZFRC.bit.OST = 0x1;
    EPwm2Regs.TZFRC.bit.OST = 0x1;
    EPwm3Regs.TZFRC.bit.OST = 0x1;
    EDIS;
}
