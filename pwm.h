
#ifndef PWM_H_
#define PWM_H_

void InitEPwmCommon(int, int);
void EnablePWM(void);
void DisablePWM(void);

#endif /* PWM_H_ */
