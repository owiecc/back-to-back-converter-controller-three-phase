
#include "DSP28x_Project.h"
#include "pwm.h"
#include "adc.h"
#include "stdbool.h"
#include "iir_filter.h"
#include "reference_frames.h"

// constants
#define PI 3.14159
#define TAU 6.283185
// PWM
#define F_SW 10000 // switching frequency
#define PRD (90000000/2/F_SW) // PWM perdiod = fosc / 2 (up-down mode) / fPWM
#define PRD2 (PRD/2) // Half of PWM perdiod
#define DB 100 // dead time in PWM counts
// converter
#define I_TRIP 70.0 // instantaneous phase current that triggers an over-current trip
#define V_DC_TRIP 700.0 // DC-link voltage that triggers an over-voltage trip
#define I_REF 50.0 // peak nominal current
#define F_0 20.0 // output waveform nominal frequency
#define L_LOAD 0.002 // load inductor phase value, star connection
// controller
#define V_TH_ON 60 // converter turns on if Vdc raises above V_TH_ON
#define V_TH_OFF 40 // converter turns off if Vdc falls below V_TH_OFF
#define VDC_I_REF 600 // converter reaches Iref at Vdc = VDC_I_REF
#define OFFSET_CYCLES 32 // number of offset cycles integrations
#define STOP_AFTER_N_CYCLES 4000 // debug only

// Function Prototypes

__interrupt void adc_isr(void);
struct dq abc2dq(struct abc,float);
struct abc dq2abc(struct dq,float);

//float loga[STOP_AFTER_N_CYCLES];
//float logb[STOP_AFTER_N_CYCLES];
//float logc[STOP_AFTER_N_CYCLES];

int main(void)
{
    // Initialize System Control: PLL, WatchDog, enable Peripheral Clocks
    InitSysCtrl();

    InitGpio();

    // Clear all interrupts and initialize PIE vector table: Disable CPU interrupts
    DINT;
    InitPieCtrl();
    IER = 0x0000;
    IFR = 0x0000;
    InitPieVectTable();
    EALLOW;
    PieVectTable.ADCINT1 = &adc_isr;
    EDIS;

    // Initialize all the device peripherals:
    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
    EDIS;

    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
    EDIS;

    //memcpy(&RamfuncsRunStart, &RamfuncsLoadStart, (Uint32)&RamfuncsLoadSize);
    //InitFlash();

    InitADC();
    InitEPwmCommon(PRD,DB);

    for(;;)
    {
        __asm("          NOP");
    }
}

__interrupt void adc_isr(void)
{
    GpioDataRegs.GPACLEAR.bit.GPIO16 = 1; // LED1 UV FAIL
    GpioDataRegs.GPACLEAR.bit.GPIO20 = 1; // LED2 OV FAIL
    GpioDataRegs.GPACLEAR.bit.GPIO30 = 1; // LED3 OC FAIL
    GpioDataRegs.GPBCLEAR.bit.GPIO51 = 1; // LED4 RUN

    GpioDataRegs.GPASET.bit.GPIO9 = 1;
    // Θ calculation

    static int cycle_ctr = 0;

    const float omega_0 = 2*PI*F_0;
    const int period_cycles = F_SW/F_0;

    static int idx_theta = 0;

    float theta = (idx_theta*TAU)/period_cycles;

    idx_theta = (idx_theta == period_cycles) ? 0 : idx_theta + 1; // increase the index by one, reset once the period is reached

    // Current reference

    struct dq idq_ref = { 0, 0 };

    // ADC conditioning

    enum sensor_offset_status {uncal,cal};
    static enum sensor_offset_status i_sens_offsets = uncal;

    static struct abc_int sensor_offsets = {0,0,0};
    struct abc_int adc_i = {0,0,0};

    int adc_0A_ref = AdcResult.ADCRESULT4;

    adc_i.a = AdcResult.ADCRESULT1 - adc_0A_ref - sensor_offsets.a; // remove ADC offset
    adc_i.b = AdcResult.ADCRESULT2 - adc_0A_ref - sensor_offsets.b; // remove ADC offset
    adc_i.c = - adc_i.a - adc_i.b;

    struct abc i_abc;

    i_abc.a = adc_i.a*0.0390625; // scale the ADC value
    i_abc.b = adc_i.b*0.0390625; // LEM HASS 50-S sensor
    i_abc.c = adc_i.c*0.0390625; // ADC_VALUE*50/0.625/2048;

    // Vdc voltage divider
    // 4*330k / 5.6k -> Vout = (R1+R2)/R2*Vin = 236.7143*Vin
    // Vin = ADCRESULT/4096*VADCREF
    // Vout = (R1+R2)/R2/4096*3.3*ADCRESULT = 0.1907
    float Vdc = 0.1907*AdcResult.ADCRESULT3;

    enum trip_source {notrip,uv,ov,oc,cyclelimit};
    static enum trip_source trip_src = notrip;

    // State machine
    //                       ┌───────┐
    //                       │       ▼
    //       Vdc > V_TH_ON ┌───────────┐
    //        ┌────────────│   idle    │◀───────────┐
    //        │            └───────────┘            │
    //        │              ┌───────┐              │
    //        ▼              │       ▼              │
    //  ┌───────────┐      ┌───────────┐      ╔═══════════╗
    //  │  startup  │      │   trip    │─────▶║ shutdown  ║
    //  └───────────┘      └───────────┘      ╚═══════════╝
    //        │                  ▲                  ▲
    //        │                  │                  │
    //        │            ┌───────────┐            │
    //        └───────────▶│    run    │────────────┘
    //                     └───────────┘ Vdc < V_TH_OFF
    //                       │       ▲
    //                       └───────┘

    enum states {idle,startup,run,shutdown,trip};
    static enum states state = trip; // initial state

    // next state logic
    switch (state) {
        case idle:
            trip_src = uv;
            sensor_offsets.a = 0; // reset sensor offsets
            sensor_offsets.b = 0;
            state = (Vdc > V_TH_ON) ? startup : idle; break;
        case startup:
            state = (i_sens_offsets==cal) ? run : startup;  break;
        case run:
            trip_src = (cycle_ctr >= STOP_AFTER_N_CYCLES) ? cyclelimit : trip_src;
            trip_src = (abs(i_abc.a) > I_TRIP || abs(i_abc.b) > I_TRIP || abs(i_abc.c) > I_TRIP) ? oc : trip_src;
            trip_src = (Vdc > V_DC_TRIP) ? ov : trip_src;
            trip_src = (Vdc < V_TH_OFF) ? ov : trip_src; // TODO debug
            state = (trip_src != notrip) ? trip : ( (Vdc < V_TH_OFF) ? shutdown : run ); break;
        case shutdown:
            state = idle; break;
        case trip:
            {
            int reset_btn = GpioDataRegs.GPADAT.bit.GPIO6; // Read BTN1 state
            state = (reset_btn == 0 && Vdc < V_TH_ON) ? shutdown : trip; // shutdown only when Vdc < ON threshold value
            }
            break;
        default:
            state = trip;
    }

    static struct iirFilter PI_d = {{6.3014,-11.4257,5.13},{1.0,-1.1429,0.1429},{0,0},{0,0}}; // init a d-axis PI controller
    static struct iirFilter PI_q = {{6.3014,-11.4257,5.13},{1.0,-1.1429,0.1429},{0,0},{0,0}}; // init a q-axis PI controller

    // state logic
    switch (state) {
       case idle:
           // UV fail LED on
           GpioDataRegs.GPASET.bit.GPIO16 = 1;
           break;
       case startup:
           {
               static int i_offset = OFFSET_CYCLES;
               if (i_offset-- == 0)
               {
                   // calculate the sensor offsets
                   sensor_offsets.a = sensor_offsets.a/OFFSET_CYCLES;
                   sensor_offsets.b = sensor_offsets.b/OFFSET_CYCLES;
                   i_offset = OFFSET_CYCLES; // reset counter
                   i_sens_offsets = cal; // offsets are now calibrated
               }
               else
               {
                   // integrate sensor offsets
                   sensor_offsets.a += AdcResult.ADCRESULT1 - adc_0A_ref;
                   sensor_offsets.b += AdcResult.ADCRESULT2 - adc_0A_ref;
               }
           }
           // reset cycle counter
           cycle_ctr = 0;
           // reset the PI controllers
           reset(&PI_d);
           reset(&PI_q);
           // reset the trip conditions
           trip_src = notrip;
           // enable the PWM signals
           EnablePWM();
           break;
       case run:
           {
           // Run LED on
           GpioDataRegs.GPASET.bit.GPIO30 = 1;
           // current reference
           float idramp = Vdc/VDC_I_REF*I_REF;
           idq_ref.d = (idramp > I_REF) ? I_REF : idramp; // ramp up the current with Vdc; saturate at I_REF
           idq_ref.q = 0;
           // control law
           struct dq i_dq = abc2dq(i_abc,theta); // abc → dq

           float vd = update(&PI_d, idq_ref.d - i_dq.d);
           float vq = update(&PI_q, idq_ref.q - i_dq.q);
           // ωL feed forward
           //vd -= omega_0*L_LOAD*i_dq.q;
           //vq += omega_0*L_LOAD*i_dq.d;
           struct dq v_dq = {vd,vq};
           struct abc v_abc = dq2abc(v_dq,theta); // dq → abc
/*
           if (cycle_ctr>=0 & cycle_ctr<STOP_AFTER_N_CYCLES){
               loga[cycle_ctr] = i_abc.a; // LOG ******************************************************
               logb[cycle_ctr] = v_dq.d;
               logc[cycle_ctr] = v_dq.q;
               }
*/
           cycle_ctr++;

           // PWM modulator
           float Vdc2 = Vdc/2;
           EPwm1Regs.CMPA.half.CMPA = (Uint16)(PRD2 + PRD2*v_abc.a/Vdc2);
           EPwm2Regs.CMPA.half.CMPA = (Uint16)(PRD2 + PRD2*v_abc.b/Vdc2);
           EPwm3Regs.CMPA.half.CMPA = (Uint16)(PRD2 + PRD2*v_abc.c/Vdc2);
           }
           break;
       case shutdown:
           DisablePWM(); // disable the PWM signals
           i_sens_offsets = uncal; // invalidate the offsets
           break;
       case trip:
           // disable the PWM signals
           DisablePWM();
           // indicate the trip condition
           switch (trip_src) {
               case uv: GpioDataRegs.GPASET.bit.GPIO16 = 1; break;
               case ov: GpioDataRegs.GPASET.bit.GPIO20 = 1; break;
               case oc: GpioDataRegs.GPBSET.bit.GPIO51 = 1; break;
           }
           break;
       default:
           state = trip;
       }

    GpioDataRegs.GPACLEAR.bit.GPIO9 = 1;

    // Clear ADCINT1 flag reinitialize for next SOC
    AdcRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;

    // Acknowledge this interrupt to receive more interrupts from group 1
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}
