
#include "reference_frames.h"
#include "math.h"

struct dq abc2dq(struct abc in, float theta)
{
    struct dq out = {0,0};

    out.d = 0.6666666666 * ( + cos(theta)*in.a + cos(theta-TAU3)*in.b + cos(theta+TAU3)*in.c);
    out.q = 0.6666666666 * ( - sin(theta)*in.a - sin(theta-TAU3)*in.b - sin(theta+TAU3)*in.c);

    return out;
}

struct abc dq2abc(struct dq in, float theta)
{
    struct abc out = {0,0,0};

    out.a = cos(theta)*in.d - sin(theta)*in.q;
    out.b = cos(theta-TAU3)*in.d - sin(theta-TAU3)*in.q;
    out.c = cos(theta+TAU3)*in.d - sin(theta+TAU3)*in.q;

    return out;
}
